//! [file]: # (src/constants.rs)
//! [author]: # (Andrew Kubera <andrewkubera@gmail.com>)
//!
//! Constants used by rest of package.
//!
//! The values in this module would be #defines or static consts
//! in the original C implementation of the protocol.
//!

/// open as read only
pub const OREAD: u16 = 0;
/// open as write only
pub const OWRITE: u16 = 1;
/// open as read-write
pub const ORDWR: u16 = 2;
/// open as executable
pub const OEXEC: u16 = 3;
/// or'ed in (except for exec), truncate file first
pub const OTRUNC: u16 = 16;

/// or'ed in, close on exec
pub const OCEXEC: u16 = 32;
/// or'ed in, remove on close
pub const ORCLOSE: u16 = 64;
/// or'ed in, direct access
pub const ODIRECT: u16 = 128;
/// or'ed in, non-blocking call
pub const ONONBLOCK: u16 = 256;
/// or'ed in, exclusive use (create only)
pub const OEXCL: u16 = 0x1000;
/// or'ed in, lock after opening
pub const OLOCK: u16 = 0x2000;
/// or'ed in, append only
pub const OAPPEND: u16 = 0x4000;
