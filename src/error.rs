//! [file]: # (styx/error.rs)
//! [author]: # (Andrew Kubera <andrewkubera@gmail.com>)
//!
//! All errors that may be encountered when using the styx protocol.
//!
//! This module contains all the protocol errors used by 9P.
//!

use std::error::Error;
use std::fmt;


#[derive(Debug, PartialEq)]
pub enum StyxError {
  BadOffset,
  Botch,
  CreateInNondir,
  DupFid,
  DupTag,
  IsDir,
  NoCreate,
  NoRemove,
  NoStat,
  NotFound,
  NoWStat,
  Perm,
  UnknownFid,
  BadDir,
  WalkNotDir,
  Open,
}

impl fmt::Display for StyxError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
  {
    write!(f, "{}", self.description())
  }
}


impl Error for StyxError {
  /// Return description string of the error
  fn description(&self) -> &str
  {
    match *self {
      StyxError::BadOffset => "bad offset",
      StyxError::Botch => "9P protocol botch",
      StyxError::CreateInNondir => "create in non-directory",
      StyxError::DupFid => "duplicate fid",
      StyxError::DupTag => "duplicate tag",
      StyxError::IsDir => "is a directory",
      StyxError::NoCreate => "create prohibited",
      StyxError::NoRemove => "remove prohibited",
      StyxError::NoStat => "stat prohibited",
      StyxError::NotFound => "file not found",
      StyxError::NoWStat => "wstat prohibited",
      StyxError::Perm => "permission denied",
      StyxError::UnknownFid => "unknown fid",
      StyxError::BadDir => "bad directory in wstat",
      StyxError::WalkNotDir => "walk in non-directory",
      StyxError::Open => "file not open",
    }
  }

  fn cause(&self) -> Option<&Error>
  {
    None
  }
}
