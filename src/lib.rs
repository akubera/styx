//! [file]: # (src/lib.rs)
//! [author]: # (Andrew Kubera <andrewkubera@gmail.com>)
//!
//! Styx is a Rust implementation of Plan9's file system protocol,
//! [9P](http://9p.cat-v.org/documentation) (also known as Styx).
//!
//! Styx allows your program to expose a file-system like interface to other
//! processes (local or networked). These file are generic endpoints to any
//! data your program wishes to share.
//!
//! The obvious use case for this library would be a file system - where
//! files exposed are the files and directories at hand. Less obvious is a
//! device driver that exposes current state (`cat /webcam/resolution # 1024x1024`)
//! or your window manager that shows all window names (`cat /wm/5/name
//! /wm/5/width # firefox800`)
//!
//! This library hopes to provide both a clean implementation for use with
//! rust projects, and an external interface for use as a low-level
//! base library for other languages.
//!
//! There are many [implementations](http://9p.cat-v.org/implementations)
//! of 9P/Styx in other languages already.
//! It should be trivial to integrate multiple projects together using
//! the protocol.
//!

extern crate bincode;

mod qid;

pub use qid::{Qid, QidFileType};

// Expose modules
pub mod message;
pub mod error;
pub mod constants;
