//! [file]: # (src/qid.rs)
//! [author]: # (Andrew Kubera <andrewkubera@gmail.com>)
//!
//! Submodule for qid values.
//!

use std::ops::{BitAnd, BitOr};

#[repr(C)]
/// The qid represents the server's unique identification for the file being
/// accessed: two files on the same server hierarchy are the same if and only
/// if their qids are the same. (The client may have multiple fids pointing
/// to a single file on a server and hence having a single qid.)
#[derive(RustcEncodable, RustcDecodable)]
pub struct Qid {
  /// Unique integer among all files in the hierarchy
  pub path: u64,

  /// Version number for a file
  pub version: u32,

  /// Mask specifying if the file is a directory, append-only file, etc
  pub qid_type: QidFileType,
}

/// Encoding of the type of a file
#[derive(RustcEncodable, RustcDecodable)]
#[repr(u8)]
pub enum QidFileType {
  /// plain file
  FILE = 0x00u8,
  /// symbolic link
  SYMLINK = 0x02u8,
  /// non-backed-up file
  TMP = 0x04u8,
  /// authentication file
  AUTH = 0x08u8,
  /// mounted channel
  MOUNT = 0x10u8,
  /// exclusive use file
  EXCL = 0x20u8,
  /// append only file
  APPEND = 0x40u8,
  /// directory
  DIR = 0x80u8,
}

impl BitAnd<u8> for QidFileType {
  type Output = u8;
  fn bitand(self, rhs: u8) -> Self::Output
  {
    self as u8 & rhs
  }
}
impl BitOr<u8> for QidFileType {
  type Output = u8;
  fn bitor(self, rhs: u8) -> Self::Output
  {
    self as u8 | rhs
  }
}

#[test]
fn test_file_typex()
{
  assert_eq!(QidFileType::DIR as u8, 0x80u8);
}
