====
Styx
====

An implementation of `Plan9's <_Plan9>`_ distributed filesystem protocol, 9P_, in the Rust_
programming language.

A library (libstyx) will be developed with the intention that other languages may bind to a
safe and efficient low-level library.

.. contents:: Table of Contents


About
-----

Developed by researchers at Bell Labs to succeed UNIX, the Plan9 operating system was designed
to be a distributed computing platform; many individual machines would comprise your computer,
each adding services such as disk space, devices, and CPU time.

To connect the components of the system (both locally and over a network) they developed 9P,
the Plan9 Filesystem Protocol.

9P is designed to provide a unified messaging interface, in the form of a filesystem, for
client-server communications.
The server, which could be any generic process providing information (everything from
databases, hardware drivers, network connections, to open winos), exposes files which
may be mounted into the client's filesystem, and from there are able to be read and (possibly)
written to.

Inferno_, an alternative operating system developed by the Plan9 team that could also
be run as a virtual machine, implemented a variant of 9P called Styx.
After the 4th release of Plan9, 9P and Inferno's Styx protocol are equivalent - this project
takes its name from the latter because it looks nicer as a project name than something with the
number '9' in it like ru9p or nine_p (no claim on the ownership of either of those names are
made here).

This is a Rust crate that provides a *rustic* interface for easily creating processes that
communicate via 9P protocol. It (will) also serve as a high-efficiency base for creating
bindings in other languages.

Bindings
--------

Work on the libstyx library has not yet begun.

Contributing
------------

You're encouraged to participate in the development of the Styx library.
The source version control is done with git, and the community is located on github.com;
feel free to add to the wiki, start a discussion, create an issue, fork and make a pull
request.
The rules are simple:

* Follow the coding conventions/styles by using the rustfmt utility on your code before
  commiting
  - The ``.editorconfig`` and ``rustfmt.toml`` configuration files should help

* Make sure all tests pass before a pull request is made
* Avoid large commits with multiple files changed and features added
* Please no 'unhelpful' commit messages - and no swearing!
* No unsafe blocks unless discussed
* Document as you go

License
-------

Licensed under the MIT_ software license.


.. === EXTERNAL LINKS === ..

.. _Inferno: http://doc.cat-v.org/inferno/
.. _9P: http://9p.cat-v.org/documentation/
.. _Rust: https://www.rust-lang.org/
.. _Plan9: http://plan9.bell-labs.com/plan9/
.. _MIT: https://opensource.org/licenses/MIT
