extern crate styx;
extern crate rustc_serialize;
extern crate bincode;

use bincode::rustc_serialize::{encode, decode};

#[test]
fn hello_struct_test()
{
  let qid = styx::Qid {
    path: 1,
    version: 42,
    qid_type: styx::QidFileType::AUTH,
  };
  assert_eq!(qid.version, 42);
  assert_eq!(qid.qid_type & 8, 8);
}

#[test]
fn test_serialization()
{
  let qid = styx::Qid {
    path: 36893488147419100,
    version: 42,
    qid_type: styx::QidFileType::AUTH,
  };

  // The maximum size of the encoded message.
  let limit = bincode::SizeLimit::Bounded(20);

  let encoded: Vec<u8> = encode(&qid, limit).unwrap();
  println!("{:?}", encoded);

  assert_eq!(encoded, vec![0, 131, 18, 110, 151, 141, 79, 220, 0, 0, 0, 42, 0, 0, 0, 3]);
}
